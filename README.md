# test-log-monitor

This is a demo for logging && monitoring on OpenShift.

For Logging: EFK
  - Elasticsearch: store loginfo
  - Fluentd: agent collect log to server, server send to ES
  - Kibana: WebUI for ES query

For Monitoring: Prometheus、Node Exporter、Grafana
  - Prometheus: collect and store metrics
  - Node Exporter: metrics exporter for machine(pod)
  - Grafana: charts for the store of Peometheus
