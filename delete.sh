#!/bin/bash

oc get is | tail -n +2 | awk '{print $1}' | xargs oc delete is
oc get bc | tail -n +2 | awk '{print $1}' | xargs oc delete bc
oc get dc | tail -n +2 |awk '{print $1}' | xargs oc delete dc
oc delete statefulsets prometheus --force --grace-period=0 --cascade=false
oc get pod | tail -n +2 | awk '{print $1}' | xargs oc delete pod
oc get route | tail -n +2 |awk '{print $1}' | xargs oc delete route
oc get service | tail -n +2 | awk '{print $1}' | xargs oc delete service
oc get pvc | tail -n +2 | awk '{print $1}' | xargs oc delete pvc
oc get pv | tail -n +2 | awk '{print $1}' | xargs oc delete pv
